from pathlib import Path


def print_string_to_file(str_out, file_name):
    output_file = Path() / "out" / file_name

    with output_file.open(mode="w", encoding="utf-8", newline="") as out_file:
        out_file.write(str_out)
