# This file is here and in the "shared-libraries" repo.
# https://gitlab.com/AuCoDe/umass-aucode/shared-libraries
# It was last updated on
#    10-17-2018 at 03:39 PM
# Compare it to the file here to verify you have the newest version.
# 💥 IF YOU MAKE CHANGES HERE ALSO MAKE THEM IN THE SHARED REPO 💥

import json
import sys
from datetime import date, timedelta
from itertools import islice
from types import SimpleNamespace


def chunked_iterable(iterable, chunk_size):
    """
    Breaks any iterable into to iterable chunks.

    https://stackoverflow.com/a/34935239/25197
    """

    # TODO: Test against these solutions for speed.
    # https://stackoverflow.com/questions/24527006/split-a-generator-into-chunks-without-pre-walking-it/
    # Possibly switch to using Boltons https://github.com/mahmoud/boltons/

    it = iter(iterable)
    while True:
        chunk = tuple(islice(it, chunk_size))
        if not chunk:
            break
        yield chunk


def chunked_dataframe(dataframe, chunk_size):
    """
    Breaks dataframe into iterable chunks.

    https://stackoverflow.com/a/49417272/25197
    """

    row_count = len(dataframe.index)
    return (dataframe[row : row + chunk_size] for row in range(0, row_count, chunk_size))


def splitlines(file, newline, chunk_size=4096):
    """
    Generate lines from file (a file-like object), where a line is
    followed by the string newline (or the end of the file). Optional
    argument chunk_size is the size of the chunks read from the file.

    https://codereview.stackexchange.com/q/205631/30306
    """
    part = ""
    while True:
        chunk = file.read(chunk_size)
        if not chunk:  # EOF
            if part:  # Left over from previous chunk.
                yield part
            break
        first, *rest = chunk.split(newline)
        part += first
        if rest:
            yield part
            yield from rest[:-1]
            part = rest[-1]


def date_range(start, stop, step=1, inclusive=False):
    """
    Given a start date and end date returns an iterable date range.

    https://stackoverflow.com/a/52509719/25197
    """
    day_count = (stop - start).days
    if inclusive:
        day_count += 1

    if step > 0:
        range_args = (0, day_count, step)
    elif step < 0:
        range_args = (day_count - 1, -1, step)
    else:
        raise ValueError("date_range(): step arg must be non-zero")

    for i in range(*range_args):
        yield start + timedelta(days=i)


def my_name(n=0):
    """
    Returns the current function or caller name.

    For current func name, specify 0 or no argument.\t
    For name of caller of current func, specify 1.\t
    For name of caller of caller of current func, specify 2. etc.
    """

    return sys._getframe(n + 1).f_code.co_name


def log_elapsed_time(elapsed_time, prefix=""):

    if elapsed_time < 60:
        print(f"{prefix}{round(elapsed_time, 2)} seconds.")
    elif elapsed_time < 60 * 10:
        print(f"{prefix}{round(elapsed_time/60, 2)} minutes.")
    else:
        print(f"{prefix}{round(elapsed_time/60)} minutes.")


class Singleton(type):
    """
    Use as a metaclass to create singleton objects.
        class ClassName(metaclass=Singleton):
            pass
    https://stackoverflow.com/a/6798042/25197
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def json_to_python(json_text):
    """
    Converts a json object to a usable python class like object.

    https://stackoverflow.com/a/35920523/25197
    """

    return json.loads(json_text, object_hook=lambda d: SimpleNamespace(**d))


def whitespace_to_singlespace(text: str) -> str:
    """
    Converts all whitespace characters (space, tab, newline, return, formfeed) to a single space.

    https://stackoverflow.com/a/1546251/25197
    """

    return " ".join(text.split())


def multispace_to_singlespace(text: str) -> str:
    """
    Converts consecutive spaces to a single space.

    https://stackoverflow.com/a/49087098/25197
    """

    while "  " in text:
        text = text.replace("  ", " ")

    return text


def is_string(obj) -> bool:
    """
    Determines if the passed in object is a string.

    https://stackoverflow.com/a/4843178/25197
    """

    return isinstance(obj, str)
