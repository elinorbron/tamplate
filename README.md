# project_name


## Install Instructions

- Open a command prompt.
- Navigate to your aucode dev directory.
- Run `git clone git@gitlab.com:elinorbron/##poject_name##.git`

---

- Open an `Anaconda Prompt` with **Administrator Access**.
- `cd` into the squabble2 directory.
- Run `conda env create`

---

- Launch **Visual Studio Code** and open the squabble2 folder.
- Select the correct Python environment.
  - `Ctl+Shft+P` to open the command pallete.
  - Type `psi` and select the `Python: Select Interpreter` command.
  - Choose the `##poject_name##` environment from the list.

---
