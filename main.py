import logging
from time import time

logging.basicConfig(format="%(asctime)s : %(levelname)s : %(message)s", level=logging.INFO)

# from sklearn.feature_extraction.text import CountVectorizer
# import numpy
# from pathlib import Path
# import lib.gollyjer_tools as gt


if __name__ == "__main__":
        start_time = time()

        #do something her

        elapsed_time = round(time() - start_time, 2)
        logging.debug(f"somethin took {elapsed_time} seconds.")